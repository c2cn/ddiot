/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2021-11-29 23:22:59
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2023-07-12 09:13:03
 */
// 站点名称
$siteName = '店滴云-让经营场所，更智能',
// 接口地址
$apiUrl = 'https://www.dandicloud.cn',
// 基础地址
$siteUrl = 'https://www.dandicloud.cn'
// 百度地图key
$bmapAk =  'sY7GGnljSvLzM44mEwVtGozS'
// unit 单商户模式，units多商户模式
$modeType =  'units'
// 集团ID
$bloc_id = 32
// 商户id
$store_id = 1