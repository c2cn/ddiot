/**
 * @Author: yaojiawen
 * @Date:   2022-12-09 17:24:08
 * @Last Modified by:   yaojiawen
 * @Last Modified time: 2022-12-12 10:56:57
 */
export default {
  text: {
    langToggle: '语言切换',
    test1: '测试1',
    test2: '测试2',
    test3: '请填写日期'
  }
}
