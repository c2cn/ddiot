/**
 * @Author: yaojiawen
 * @Date:   2022-12-09 17:23:38
 * @Last Modified by:   yaojiawen
 * @Last Modified time: 2022-12-12 10:56:45
 */
export default {
  text: {
    langToggle: 'language-toggle',
    test1: 'ceshi1',
    test2: 'ceshi2',
    test3: 'chose date'
  }
}
