/**
 * @Author: Wang chunsheng  email:2192138785@qq.com
 * @Date:   2022-11-08 09:02:25
 * @Last Modified by:   Wang chunsheng  email:2192138785@qq.com
 * @Last Modified time: 2023-03-10 17:44:11
 */

const wechat = {
  appid: 'wx31d2dfbb57b2931f',
  // 站点基础地址 末尾不带斜杠
  // siteUrl: 'https://dev.hopesfire.com',
  redirect_uri: 'https://www.mlyvip.cn/admin/#/bea_cloud/default/index.vue'
}

module.exports = wechat
