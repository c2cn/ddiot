# 介绍

#### 开发工具与第三方文档


## 企业版saas
https://www.dandicloud.cn/
## 开发者手册
http://doc.ddicms.com/
## 开发者社区
http://dandicloud.com/

### 官方仓库：	

## 店滴CMS开源版
    [https://gitee.com/wayfirer/diandicms](https://gitee.com/wayfirer/diandicms)
## 中后台，基于element-ui:
  	[https://gitee.com/wayfirer/diandi-element-admin](https://gitee.com/wayfirer/diandi-element-admin)
## uniapp :
    [https://gitee.com/wayfirer/diandi-fireui](https://gitee.com/wayfirer/diandi-fireui)
## 数据处理中心（后端）：   
    [https://gitee.com/wayfiretech_admin/diandi-admin](https://gitee.com/wayfiretech_admin/diandi-admin)


### 开发辅助：

开发文档：[https://gitee.com/wayfiretech_admin/dandi-doc](https://gitee.com/wayfiretech_admin/dandi-doc)
开发社区：[https://www.hopesfire.com/](https://www.hopesfire.com/)
中后台表单:[diy:http://diy.hopesfire.com/](diy:http://diy.hopesfire.com/)


### 相关第三方：
[https://element.eleme.cn/#/zh-CN/component/installation](https://element.eleme.cn/#/zh-CN/component/installation)
[https://uniapp.dcloud.io/](https://uniapp.dcloud.io/)
[https://www.npmjs.com/package/vue-ele-form](https://www.npmjs.com/package/vue-ele-form)

## 主要特性
配置灵活，权限管理清晰。分总后台权限和各商户后台权限，可分应用授权。
表单灵活，一句话写万能表单，依赖json格式的数据配置
检索方便，支持各种检索类型，依赖json配置
列表数据通过简单的json配置，就可以渲染数据
分栏式左侧菜单，用户点击更便捷方便

## 在线演示

    https://dev.ddicms.cn/backend/#/login?redirect=%2F 账户 admin 密码 12345678

## 问题反馈

    https://gitee.com/wayfirer/diandi-element-admin/issues

## 特别鸣谢

    https://element.eleme.cn/
    https://panjiachen.gitee.io/vue-element-admin-site/zh/


## 版权信息

    本软件可免费商用